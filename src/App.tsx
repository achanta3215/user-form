import React from 'react';
import logo from './logo.svg';
import './App.css';
import UserForm from './user/UserForm';

function App() {
  return (
    <UserForm />
  );
}

export default App;
