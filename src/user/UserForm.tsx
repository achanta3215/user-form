import React, { useCallback, useRef, useState } from 'react';
import { USER_IDS } from '../constants';
import axios from 'axios';
import { IUserDetails } from './types';
import UserDetails from './UserDetails';

const getImageFile = (userImageRef: React.RefObject<HTMLInputElement>) => (
  userImageRef.current &&  userImageRef.current.files && userImageRef.current.files[0]
);

const UserForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [website, setWebsite] = useState('');
  const [phone, setPhone] = useState('');
  const [company, setCompany] = useState('');
  const [userData, setUserData] = useState<Partial<IUserDetails>>({}); 
  const handleUserIdSelected = useCallback<React.ChangeEventHandler<HTMLSelectElement>>((event) => {
    const selectedUserId = event.target.value;
    async function getUserDetails() {
      const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${selectedUserId}`);
      setUserData(response.data as IUserDetails);
      setName('')
      setEmail('')
      setWebsite('')
      setPhone('')
      setCompany('')
    }
    getUserDetails()

  }, [setName, setEmail, setWebsite, setPhone, setCompany]);
  const userImageRef = useRef<HTMLInputElement>(null);
  const handleSubmit = useCallback(() => {
    console.log(`name: ${name || userData.name}\nemail: ${email || userData.email}`)
    console.log(`File data: ${userImageRef.current && userImageRef.current.files && userImageRef.current.files[0]}`)
  }, [name, email, website, phone, company, userData, userImageRef]);
  const imgSrc = getImageFile(userImageRef);
  const [forceRefresh, setForceRefresh] = useState(false);
  return (
      <div style={{ padding: '20px' }}>
        <span style={{ paddingRight: '8px' }}>
          Select a User ID:
        </span>
        <select onChange={handleUserIdSelected}>
            <option value=''>-- Select --</option> 
            {USER_IDS.map(userId => <option value={userId}>{userId}</option>)}
        </select>
        {/* {userData && <UserDetails userData={userData} />} */}
        <div style={{ lineHeight: 1.8 }}>
      <div>
        <input onChange={(e) => setName(e.target.value)} value={name || userData.name} />
      </div>
      <div>
        <input onChange={(e) => setEmail(e.target.value)} value={email || userData.email} />
      </div>
      <div>
        <input onChange={(e) => setWebsite(e.target.value)} value={website || userData.website} />
      </div>
      <div>
        <input onChange={(e) => setPhone(e.target.value)} value={phone || userData.phone} />
      </div>
      <div>
        <input onChange={(e) => setCompany(e.target.value)} value={company || userData.company && userData.company.name} />
      </div>
      <div>
       <label style={{ paddingRight: '8px' }}>Upload a picture</label>
        <input
          type="file"
          accept="image/*"
          ref={userImageRef}
          onChange={() => setForceRefresh(_forceRefresh => !_forceRefresh)}
        />
          {/* <input type="file" accept="image/*" capture="user" /> */}
          { imgSrc && <img height="100" width="100" src={URL.createObjectURL(imgSrc)} />}
      </div>
      <div>
        <button type='button' onClick={handleSubmit}>Submit</button>
      </div>
    </div>
      </div>
  );
};

export default UserForm;
